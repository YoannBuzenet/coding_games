const readline = () => "salut"

// Les poissons sont effrayés
// Ils quittent la map
// Ils ont une couleur et un type

const DEPTH_BY_TYPE = {
    1 : {
        min : 2500,
        max: 5000
    },
    2 : {
        min : 5000,
        max: 7500
    },
    3 : {
        min : 7500,
        max: 10000
    },
}

class Vec2 {  
    public x: number;
    public y: number;
  
    constructor(x: number, y: number) {
      this.x = x;
      this.y = y;
    }
  
    get length() {
      return Math.sqrt(this.x ** 2 + this.y ** 2);
    }
  
    dist(o: Vec2) {
      return this.sub(o).length;
    }
  
    add(o: Vec2) {
      return new Vec2(this.x + o.x, this.y + o.y);
    }
  
    sub(o: Vec2) {
      return new Vec2(this.x - o.x, this.y - o.y);
    }
  
    divide(num: number) {
      return new Vec2(this.x / num, this.y / num);
    }
  
    times(num: number) {
      return new Vec2(this.x * num, this.y * num);
    }
}

class Drone {

    LIGHT_OFF = 0;
    LIGHT_ON = 1;

    constructor(public droneId:number,public droneX:number,public droneY:number,public emergency:number,public battery:number){}

    move(x: number, y: number){
        return `MOVE ${this.droneX + x} ${this.droneY + y}`
    }

    goToSurface(){
        console.error("Going to surface !")
        return `MOVE ${this.droneX} ${this.droneY - 600}`
    }

    goTL(){
        return `MOVE ${this.droneX - 300} ${this.droneY - 300}`
    }

    goTR(){
        return `MOVE ${this.droneX + 300} ${this.droneY - 300}`
    }

    goBL(){
        return `MOVE ${this.droneX - 300} ${this.droneY + 300}`
    }

    goBR(){
        return `MOVE ${this.droneX + 300} ${this.droneY + 300}`
    }

    controlLights(i: number){
        const isModulo3 = (number: number) => number %3 === 0;
        const isModulo5 = (number: number) => number %5 === 0;        
        
        const isActivated = (i:number) => isModulo3(i);

        if(isActivated(i)){
            console.error("Light is ON !")
        }

        return ` ${isActivated(i)? this.LIGHT_ON : this.LIGHT_OFF}`
    }
}

class Creature {
    constructor(public creatureId:number,public creatureX:number,public creatureY:number,public creatureVx:number,public creatureVy:number){}
}

class RadarBlip{
    constructor(public droneId: number, public CreatureId: number, public radar: string){}
}

class Scan {
    constructor(public droneId: number, public CreatureId: number){}
}

class CreatureDefinition {
    constructor(public CreatureId: number, public color: number, public type: number){}
}

class GameData {
    constructor(public turnNumber:number  = 0){}
}



/**
 * Score points by scanning valuable fish faster than your opponent.
**/
let allCreatureInField:CreatureDefinition[] = [];
const creatureCount = parseInt(readline());
for (let i = 0; i < creatureCount; i++) {
    var inputs = readline().split(' ');
    const creatureId = parseInt(inputs[0]);
    const color = parseInt(inputs[1]);
    const type = parseInt(inputs[2]);
    allCreatureInField.push(new CreatureDefinition(creatureId, color, type))
}

let currentGameData = new GameData();
const MAX_CREATURE = allCreatureInField.length;
const mapOfInstructions = {
    0 : {
        targetedBlip : null,
        targetCreatureId : null,
    },
    1 : {
        targetedBlip: null,
        targetCreatureId : null
    }
}

console.error("allCreatureInField",allCreatureInField)
// game loop
while (true) {


    // Funcs

    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min)
      }

    const updateRadar = (creatureId: number): RadarBlip => {
        return radarsBlip.find(radarBlip => radarBlip.CreatureId === creatureId) as RadarBlip
    }

    function isCreatureAlreadyTargeted(creatureId: number){
        console.error("isCreatureAlreadyTargeted")
        console.error("creatureId",creatureId)
        console.error('mapOfInstructions[0].targetCreatureId === creatureId || mapOfInstructions[1].targetCreatureId === creatureId',mapOfInstructions[0].targetCreatureId === creatureId || mapOfInstructions[1].targetCreatureId === creatureId)

        return mapOfInstructions[0].targetCreatureId === creatureId || mapOfInstructions[1].targetCreatureId === creatureId
    }

    function getRandomUnscannedBlipOfBlippedCreatures(): RadarBlip | null{
        for(let i = 0; i < radarsBlip.length;i++){
            const currentCreature = radarsBlip[randomIntFromInterval(0,MAX_CREATURE+1)];
            if(currentCreature && !allIDCreatureSeen().includes(currentCreature.CreatureId) && !isCreatureAlreadyTargeted(currentCreature.CreatureId)){
                return currentCreature;
            }
        }
        return null
    }

    function getNewTarget(){
        console.error("Updating target ...")
        return getRandomUnscannedBlipOfBlippedCreatures();
    }

    function hasCreatureBeenScanned (creatureId: number){
        const hasBeenSeenInSavedScans = creaturesIdAlreadySeen.includes(creatureId);
        const hasBeenSeenInNonSavedScans = nonSavedScans.filter(scan => scan.CreatureId === creatureId).length > 0;

        return hasBeenSeenInSavedScans || hasBeenSeenInNonSavedScans;
    }

    function isFishStillInGame(creatureId: number){
        return allCreatureInField.filter(creature => creature.CreatureId === creatureId).length > 0
    }


    // Store


    let creaturesIdAlreadySeen:number[] = [];
    let visiblesCreatures: Creature[] = []
    let radarsBlip: RadarBlip[] = []
    let nonSavedScans: Scan[] = []
    let drone: Drone | null= null;
    let ennemyDrone: Drone | null= null;
    let ennemyCreaturesIdAlreadySeen:number[] = [];
    let allIDCreatureSeen = () => [...nonSavedScans.map(scan => scan.CreatureId), ...creaturesIdAlreadySeen]


    // Engine

    const myScore = parseInt(readline());
    const foeScore = parseInt(readline());
    // Les scans déjà validés
    const myScanCount = parseInt(readline());
    for (let i = 0; i < myScanCount; i++) {
        const creatureId = parseInt(readline());
        creaturesIdAlreadySeen.push(creatureId);
    }
    const foeScanCount = parseInt(readline());
    for (let i = 0; i < foeScanCount; i++) {
        const creatureId = parseInt(readline());
        ennemyCreaturesIdAlreadySeen.push(creatureId);
    }
    const myDroneCount = parseInt(readline());
    for (let i = 0; i < myDroneCount; i++) {
        var inputs = readline().split(' ');
        const droneId = parseInt(inputs[0]);
        const droneX = parseInt(inputs[1]);
        const droneY = parseInt(inputs[2]);
        const emergency = parseInt(inputs[3]);
        const battery = parseInt(inputs[4]);
        drone= 
            new Drone(droneId,droneX,droneY,emergency,battery)
        
    }
    const foeDroneCount = parseInt(readline());
    for (let i = 0; i < foeDroneCount; i++) {
        var inputs = readline().split(' ');
        const droneId = parseInt(inputs[0]);
        const droneX = parseInt(inputs[1]);
        const droneY = parseInt(inputs[2]);
        const emergency = parseInt(inputs[3]);
        const battery = parseInt(inputs[4]);
        ennemyDrone= 
            new Drone(droneId,droneX,droneY,emergency,battery)
    }
    // Scans à remonter !!! TODO
    const droneScanCount = parseInt(readline());
    for (let i = 0; i < droneScanCount; i++) {
        var inputs = readline().split(' ');
        const droneId = parseInt(inputs[0]);
        const creatureId = parseInt(inputs[1]);
        const scan = new Scan(droneId, creatureId);
        nonSavedScans.push(scan);
    }
    const visibleCreatureCount = parseInt(readline());
    for (let i = 0; i < visibleCreatureCount; i++) {
        var inputs = readline().split(' ');
        const creatureId = parseInt(inputs[0]);
        const creatureX = parseInt(inputs[1]);
        const creatureY = parseInt(inputs[2]);
        const creatureVx = parseInt(inputs[3]);
        const creatureVy = parseInt(inputs[4]);
        const creature = new Creature(
            creatureId,creatureX,creatureY,creatureVx,creatureVy
        )
        visiblesCreatures.push(creature)
    }
    const radarBlipCount = parseInt(readline());
    for (let i = 0; i < radarBlipCount; i++) {
        var inputs = readline().split(' ');
        const droneId = parseInt(inputs[0]);
        const creatureId = parseInt(inputs[1]);
        const radar = inputs[2];
        const radarBlip = new RadarBlip(droneId, creatureId,radar)
        radarsBlip.push(radarBlip)
    }


    for (let i = 0; i < myDroneCount; i++) {

        console.error(`-----------------`)
        console.error(`drone n°${i}`)
        
        if(currentGameData.turnNumber > 184){
            console.log(`${drone?.goToSurface()} ${drone?.controlLights(currentGameData.turnNumber)}`);
        }
        else{
            // Choper une target si on en a pas
            // sinon, rester sur la target    
            if(!mapOfInstructions[i].targetedBlip?.CreatureId || !isFishStillInGame(mapOfInstructions[i].targetedBlip?.CreatureId) && isCreatureAlreadyTargeted(mapOfInstructions[i].targetedBlip?.CreatureId)){
                // get cretureId
                mapOfInstructions[i].targetedBlip = getNewTarget()
            }
            
            const hasBeenChecked = hasCreatureBeenScanned(mapOfInstructions[i].targetedBlip?.CreatureId as number)
            
            if(hasBeenChecked){
                // go in direction of that creature
                mapOfInstructions[i].targetedBlip = getNewTarget()
            }
            
            mapOfInstructions[i].targetedBlip = updateRadar(mapOfInstructions[i].targetedBlip?.CreatureId as number)
            
            console.error('Targeted id', mapOfInstructions[i].targetedBlip?.CreatureId)
            console.error("Seen stuff", allIDCreatureSeen())
            console.error('hasBeenChecked',hasBeenChecked)
            console.error('direction :', mapOfInstructions[i].targetedBlip?.radar)

            if(mapOfInstructions[i].targetedBlip?.radar){
                const droneMove = (drone as Drone)[`go${mapOfInstructions[i].targetedBlip?.radar}`]();
                console.log(droneMove+(drone as Drone).controlLights(currentGameData.turnNumber))
            }
            else{
                console.log(`${drone?.goToSurface()}${drone?.controlLights(currentGameData.turnNumber)}`);
            }
    }
    }
    currentGameData.turnNumber++
}
